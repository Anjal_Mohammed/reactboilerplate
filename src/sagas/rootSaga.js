import { all } from 'redux-saga/effects'
import { watchIncrementAsync } from "./counterSaga";
import { apiCallWatcher } from './apiSaga';

export default function* rootSaga() {
    yield all([
        watchIncrementAsync(),
        apiCallWatcher(),
    ])
}