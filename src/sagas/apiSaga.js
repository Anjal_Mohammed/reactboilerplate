import { takeEvery, call } from 'redux-saga/effects'
import sdk from '../utils/SlaveBot';
import { message } from 'antd';

export function* makeApiCall() {
    try {
        const appsdk = sdk.initialize();
        const response = yield call(() => appsdk.api.getBreeds({ animal_type: 'cat' }));
        const { status } = response;
        if (status === 200) {
            message.success('Api call successful..')
        }
    }
    catch (err) {
        console.error(err)
    }
}

export function* apiCallWatcher() {
    yield takeEvery('MAKE_API_CALL', makeApiCall)
}