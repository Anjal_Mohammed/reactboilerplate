import { put, takeEvery } from 'redux-saga/effects'
import { increment, buttonLoader } from '../actions/counterActions'

const delay = (ms) => new Promise(res => setTimeout(res, ms))

export function* incrementAsync() {
    try {
        yield put(buttonLoader(true))
        yield delay(3000)
        yield put(increment())
    }
    catch (err) {
        console.error(err)
    }
    finally {
        yield put(buttonLoader(false))
    }
}

export function* watchIncrementAsync() {
    yield takeEvery('ASYNC_INCREMENT', incrementAsync)
}