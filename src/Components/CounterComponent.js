import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Button } from 'antd';

import { increment, decrement, asyncIncrement } from '../actions/counterActions';
import { makeApiCall } from '../actions/apiActions';
import { StyledButton } from '../styles/StyledComponents';

const CounterComponet = props => {

    useEffect(() => {
        console.log('api call triggered on mount')
        props.makeApiCall()
    }, []);

    return (
        <>
            <p>Counter value changes by using redux</p>
            <div className='flex items-center'>
                <Button loading={props.loader} onClick={props.increment}>+</Button>
                <p className='mb0 mx2'>Value: {props.counter}</p>
                <Button loading={props.loader} onClick={props.decrement}>-</Button>
            </div>
            <div>
                <Button className='my1' loading={props.loader} onClick={props.asyncIncrement}>Click to increment with delay of 3s</Button>
                <StyledButton className='my1 ml1' loading={props.loader}
                    onClick={() => props.history.push('/another_route')}>Click to go to different route</StyledButton>
            </div>
            <h2 className='mt2'>Open Network tab to see api calls happening</h2>
        </>
    );
}

const mapStateToProps = ({
    counter,
    loader: { loader }
}) => ({
    counter,
    loader
})

const mapDispatchToProps = dispatch => ({
    increment: () => dispatch(increment()),
    decrement: () => dispatch(decrement()),
    asyncIncrement: () => dispatch(asyncIncrement()),
    makeApiCall: () => dispatch(makeApiCall())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CounterComponet));