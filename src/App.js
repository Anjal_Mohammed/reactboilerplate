import React from 'react';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './reducers/rootReducer';
import rootSaga from './sagas/rootSaga';

import "./styles/index.less";
import CounterComponent from './Components/CounterComponent';
import Routes from './Routes';

let sagaMiddleware = createSagaMiddleware();
let store = createStore(rootReducer,
    applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

const App = () => (
    <Provider store={store}>
        <div>
            <h1>Hello World, React!</h1>
            <Routes/>
        </div>
    </Provider>
)

export default App;