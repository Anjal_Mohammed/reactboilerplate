export const increment = () => ({
    type: 'INCREMENT'
});

export const decrement = () => ({
    type: 'DECREMENT'
});

export const asyncIncrement = () => ({
    type: 'ASYNC_INCREMENT'
});

export const buttonLoader = triggerValue => ({
    type: 'TOGGLE_LOADER',
    payload: triggerValue
});