import { combineReducers } from "redux";

//reducers
import countReducer from './countReducer';
import loaderReducer from './loaderReducer';

const rootReducer = combineReducers({
    loader: loaderReducer,
    counter: countReducer
})

export default rootReducer;