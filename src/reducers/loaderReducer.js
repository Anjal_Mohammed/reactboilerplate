const initialState = {
    loader: false
}

const loaderReducer = function (state = initialState, { type, payload }) {
    const updateState = updation => Object.assign({}, state, updation);

    switch (type) {
        case "TOGGLE_LOADER":
            return updateState({ loader: payload });
        default:
            return state;
    }
};

export default loaderReducer;